[TOC]

# 2022-09-15 Einführung, Markdown Versionsverwaltung

- [x] Beurteilung  
    - [x] anhand Projekt  
    - [x] (MA)aktive Mitarbeit -> +/-  
    - [x] (MA) Hausübungen, SMÜ  
    - [x] Note -> 50% MA, 50% Projekt  
>MA und Projekte müssen positiv sein

  
## Markdown  
Aufzählungen  
~~~  
-item 1  
-item 2  
~~~  
-item 1  
-item2

### Nummerierungen  
~~~  
1. item 1  
2. item 2  
~~~

1. item 1  
1. item 2

### Überschriften  
~~~  
# Ebene 1  
## Ebene 2  
~~~

### Textformatierungen  
~~~  
Franz *jagt* im **komplett** verwahr _losten_ Taxi  
~~~

Franz *jagt* im **komplett** verwahr _losten_ Taxi

### blockquote  
Für Zitate, Definitionen,...  
~~~  
> Zitat  
~~~  
> Zitat

### Code im Text  
Es ist auch möglich, im Text `Console.Writeline("Hello")`  
anzugeben

### Hypertext im Markdown Code  
~~~  
<h2>Überschrift 2 </h2>  
~~~  
Es ist auch möglich, im markdown Hypertexte für bestimmte Ausnahmefälle anzugeben.

### Abgrenungslinien  
---  
Franz

---

### Einfügen von Abbildungen

![[711e5b6481fb4f65.jpg]]

### Syntaxhighlighting von Code  
~~~python  
// Ein HelloWorld in python  
printf("Hello World.");  
~~~

  
~~~c  
// Ein HelloWorld in c  
printf("Hello World.");  
~~~

  
~~~cs  
// Ein HelloWorld in c#  
Console.Writeline("Hello World.");  
~~~

  
| Datum      | Text       |  
| ---------- | ---------- |  
| 2022-09-15 | markdown   |  
| 2022-09-15 | **gitlab** |  
| 😄         | 👎         |  
|            |            |

  
### Diagramme  
[https://mermaid-js.github.io](https://mermaid-js.github.io/ "https://mermaid-js.github.io")

  
~~~mermaid  
flowchart TD  
    Start --> Stop  
    Aufstehen ---> Kaffee  
    Aufstehen ---> Start  
~~~

# 2022-09-22 Einführung Versionsverwaltung

## Beispiele Für Versionsverwaltungen  
1. git  
2. svn... subversion  
3. mercurial  
4. cvs  
5. treamsfoundation

## Plattformen  
1. github.com -> MS  
2. gitlab.com -> selber hosting möglich -> freeware für grundlegene Funktionen  
3. bitbucket ->  
4. ...

## Grundlegende Bash- Befehle

| Befehl      | Beispiel     | Erklärung                                                                                                      |
| ----------- | ------------ | -------------------------------------------------------------------------------------------------------------- |
| promt       |              | Hinweis, dass jetzt ein Befehl eingegeben werden kann, kann unterschiedlich ausschauen und lässt sich anpassen |
| ls          |              | gibt Inhalt des aktuellen Verzeichnisses aus                                                                   |
| ~           |              | Homeverzeichniss                                                                                               |
| pwd         |              | print working directory, gibt den Pfad des aktuellen Verzeichniss aus                                          |
| `cd <pfad>` | cd </c/temp> | change directory                                                                                               |
| mkdir       | mkdir        | Bsp: erstellt im aktuelen Ordner ein subdir repos                                                              |
| git         |              |  des repositorys- definiert über <URL> - wird in das Lokale                                                                                                              |
| clone            |              |                                            Dateisystenm kopiert (geclont)                                                                    


~~~bash
cd
# wechseln in unser home verzeichnis
mkdir repos
# erstellen des Ordners "repos"
cd repos
 # wechseln in "repos" 
# clone repo git clone https://gitlab.com/basti-debug/wfsst_4ahel_wolf_2022.git 
# Zugriff auf die repository mit hilfe einens Schlüssels
# dazu muss EINMAL ein Schlüssel generiert werden und auf gitlab hinterlegt werden
ssh-keygen 
# öffentlichen Schlüssel anzeigen 
cat ~/.ssh/id_rsa.pub 
# jetzt wird diese Anziege in das clipboard kopiert und auf 
# gitlab.com im eigenen Profil unter ssh-keys eingefügt
cat # nun kann die repo über ssh (und somit ohne Authentifizierung über UID+PW) geladen weden git clone git@gitlab.com:basti-debug/wfsst_4chel_mayrhofer_2022.git
git status 
#gibt Änderungen in der Datei an 
git add . 
#fügt alle Dateien zum repository
vi 
#Datei im Editor öffnen
cd ... 
#eine Dateiebene höhergehen
ll
#deinstallierte Infos zu allen Dateien im Ordner wiedeegeben
touch <Datei>
#auf eine Datei "Fokussieren"
git add . 
#fügt alle Dateien zum repository hinzu(Punkt steht für alle Dateien)
git commit -am "commit message"
#hinzufügen einer Nachricht
git push
#Datei auf Gitlab hochladen
~~~

DataKontext ist für alle klassen in einem Program das gleiche
