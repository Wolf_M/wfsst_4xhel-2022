﻿using ShapesLip;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BindingToPOCOS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        public Square mySquare { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            mySquare = new Square() { Side = 335.45, Location = new System.Drawing.Point(33, 55)};

            DataContext = mySquare;
        }

        private void sldSide_MouseUp(object sender, MouseButtonEventArgs e)
        {
            
        }

        private void sldSide_LostFocus(object sender, RoutedEventArgs e)
        {
            ////russische Variante -> der DataKontext wird geändert, und so muss das GUI 
            ////den aktuellen Wert wieder holen
            
            //DataContext = null;
            //DataContext = mySquare;

            //wird eleganter gelöst, indem das GUI über eine Propertyänderung
            //informiert wird und sich das GUI somit aktualisieren kann
        }
    }
}
