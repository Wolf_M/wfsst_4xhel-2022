﻿using System;
using System.ComponentModel;
using System.Drawing;

namespace ShapesLip
{
    public class Square : INotifyPropertyChanged
    {
        private double side;

        public Point Location { get; set; }

        public double Side
        {
            get { return side; }
            set { side = value;
                OnPorpertyChanged("Area");
                OnPorpertyChanged("Side");

            }
        }
        public double Area
        {
            get { return side * side; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        internal virtual void OnPorpertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));

            //var tempHandler = PropertyChanged;
            //if (tempHandler!= null)
            //{
            //    tempHandler?.Invoke(this, new PropertyChangedEventArgs(property));
            //}
        }
    }
}
