# Wiederholungsbeispiel Fuhrpark
## Klassendiagramme

~~~plantuml
@startuml
skinparam classAttributeIconSize 0
class Vehicle 
{
-licensePlate : string
-model : string
-totalDist : double
-fuellevel : double
-available : double
-location : string
__
+LicensePlate {set, get} : string
+Model {get} : string
+TotalDist {set, get} : double
+Fuellevel {set, get} : double
+Available {set, get} : double
+Location {set, get} : string
__
+ Vehicle(ls, mod, totDist, fuell, avail, loc)

+ ToString()
+ Serialize() : string
+{static} Parse(data : string) : Vehicle
}
@enduml
~~~

~~~plantuml
	@startuml
	skinparam classAttributeIconSize 0
	class Vehicle
	{
	- items : List<Vehicle>
	--
	+ Items{get} : List<Vehicle>
	--
	+Vehicle()
	+Save(filename:string) : void
	+Open(filename:string) : void
	}
	@enduml
~~~
