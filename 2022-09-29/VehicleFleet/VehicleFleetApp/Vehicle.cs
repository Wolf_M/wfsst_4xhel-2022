﻿namespace VehicleFleetApp
{
    class Vehicle
    {
        #region Fields
        private string licenseplate;

        private string location;

        private double fuelLevel;

        private bool isAvailable;

        private string model;

        private double totalDist;

        #endregion
        // _______________________________________________________--

        #region Properties
        public double FuelLevel
        {
            get
            {
                return fuelLevel;
            }
            set
            {
                fuelLevel = value;
            }
        }

        public double TotalDist
        {
            get
            {
                return totalDist;
            }
            set
            {
                totalDist = value;
            }
        }

        public bool IsAvailable
        {
            get
            {
                return isAvailable;
            }
            set
            {
                isAvailable = value;
            }
        }

        public string Location
        {
            get
            {
                return location;
            }
            set
            {
                location = value;
            }
        }

        public string LicensePlate
        {
            get
            {
                return licenseplate;
            }
            set
            {
                licenseplate = value;
            }
        }

        public string Model
        {
            get
            {
                return model;
            }
        }

        #endregion

        //_____________________________________

        #region Constructor

        public Vehicle(string licenseplate, string location, double fuelLevel, bool isAvailable, string model, double totalDist)
        {
            this.licenseplate = licenseplate;
            this.location = licenseplate;
            this.fuelLevel = fuelLevel;
            this.isAvailable = isAvailable;
            this.model = model;
            this.totalDist = totalDist;
        }
        #endregion

        //_______________________________________

        #region Methods
        public override string ToString()
        {
            return "Kennzeichen: ";
        }

        public string Serialize()
        {
            return "";
        }

        //public static Vehicle Parse(string data)
        //{
        //    return new Vehicle();
        //}

        #endregion
    }
}
